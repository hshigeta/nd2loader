// --- Options ---

#define MAX_FILE_NAME_LENGTH   1024
#define MAX_SECTION_NAME       4096
#define BUF_SIZE              65536

// ----------------

#define SEQDATA_IGNORE            8

struct nd2im {
  char name[MAX_SECTION_NAME];
  unsigned long offset;
  long size;
};

struct nd2info {
  char filename[MAX_FILE_NAME_LENGTH];
  int X;
  int Y;
  int C;
  int Z;
  int T;
  int BitsPerPixel;
  int PixelSize;
  int image_num;
  nd2im* imlist;
};

nd2info* ParseNd2(const char*, int, int, int, int, int, int, int);
void ShowNd2info(nd2info*);
void FreeNd2(nd2info*);
