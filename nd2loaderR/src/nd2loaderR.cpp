#include <Rcpp.h>
#include <stdlib.h>
using namespace Rcpp;

#include "../../nd2loader.cpp"

nd2info* info = NULL;
int show_hints = 0;

int isNd2loadedWithMsg()
{
  if(info != NULL) {
    return 1;
  }
  printf("nd2 file is not loaded\n");
  return 0;
}

void FreeNd2R();

//' Set the Debug Mode
//' @description Configure whether printing debug info.
//' @param mode 0: debug output off, 1: debug output on. (default: 0)
// [[Rcpp::export]]
void Nd2SetDebugModeR(int mode)
{
  Nd2SetDebugMode(mode);
}

//' Set Hint Messages
//' @description Configure whether printing hints to use this library.
//' @param mode 0: hint output off, 1: hit output on. (default: 1)
// [[Rcpp::export]]
void Nd2SetHintR(int mode)
{
  show_hints = mode;
}

//' Parse the nd2 File
//' @description Parse a nd2 file and obtain the structure and meta information of the file. Actual image data is not loaded at this method.
//' @param filename    filename of the nd2 file to load.
// [[Rcpp::export]]
void ParseNd2R(const char* filename)
{
  if(info != NULL) {
    FreeNd2R();
  }
  info = ParseNd2(filename);
  if(info != NULL) {
    if(show_hints) {
      printf("nd2 file '%s' loaded\n", filename);
      printf("HINT: Now, let's load raw data with (Z, T)\n");
      printf("Example) raw <- LoadRawDataR([Z], [T])\n");
    }
  } else {
    printf("Error occured\n");
  }
}

//' Parse the nd2 File With Parameters
//' @description Parse a nd2 file with given parameters and obtain the structure of the file. Actual image data is not loaded at this method. This method can be used when automatic detection of meta data does not work correctly.
//' @param filename    filename of the nd2 file.
//' @param X,Y,C,Z,T   information of the nd2 file to load.
//' @param BitsPerPixel bits for each pixel. Typically 8, 12 or 16.
//' @param PixelSize   bytes for each pixel. Typically 1 when BitsPerPixel = 8, 2 when BitsPerPixel = 8 or 16
// [[Rcpp::export]]
void ParseNd2WithMetaInfoR(const char* filename, int X, int Y, int C, int Z, int T, int BitsPerPixel, int PixelSize)
{
  if(info != NULL) {
    FreeNd2R();
  }
  info = ParseNd2WithMetaInfo(filename, X, Y, C, Z, T, BitsPerPixel, PixelSize);
  if(info != NULL) {
    if(show_hints) {
      printf("nd2 file '%s' loaded\n", filename);
      printf("HINT: Now, let's load raw data with (Z, T)\n");
      printf("Example) raw <- LoadRawDataR([Z], [T])\n");
    }
  } else {
    printf("Error occured\n");
  }
}

//' Show the nd2 Information
//' @description Show the nd2 Information parsed by ParseNd2R.
// [[Rcpp::export]]
void ShowNd2infoR()
{
  if(isNd2loadedWithMsg()) {
    ShowNd2info(info);
  }
  return;
}

//' Load Raw Data by an Index
//' @description Load raw image data from the nd2 file by an index.
//' @param index a serial index of the nd2 file.
//' @return Loaded raw data (XYC binary vector).
// [[Rcpp::export]]
RawVector LoadRawDataIndexR(int index)
{
  RawVector data_null;

  if(!isNd2loadedWithMsg()) {
    return data_null;
  }

  // load RawData from the file
  unsigned char* d = LoadRawDataIndex(info, index);
  if(d == NULL) { // info or index is not correct
    printf("failed to get the raw data\n");
    return data_null;
  }

  // copy C type data to R type type
  int size = info->imlist[index - 1].size - SEQDATA_IGNORE;
  RawVector data(size);
  memcpy(data.begin(), d, size);
  free(d);

  if(show_hints) {
    printf("HINT: Next, get an image matrix from the raw byte array\n");
    printf("Example) im_ch <- ExtractSingleChannelR(raw, [CHANNEL])\n");
  }

  return data;
}

//' Load Raw Data Using Z and T
//' @description Load raw data from the nd2 file by Z and T.
//' @param Z,T information to load images.
//' @return Loaded raw data (XYC binary vector).
// [[Rcpp::export]]
RawVector LoadRawDataR(int Z, int T)
{
  RawVector data_null;

  if(!isNd2loadedWithMsg()){
    return data_null;
  }

  if(Z <= 0 || Z > info->Z) {
    printf("Z out of range\n");
    return data_null;
  }
  if(T <= 0 || T > info->T) {
    printf("T out of range\n");
    return data_null;
  }

  return LoadRawDataIndexR((T - 1) * info->Z + (Z - 1) + 1);
}

//' Extract a Single Channel
//' @description Extract a single channel from a raw image data and return it as a numeric matrix.
//' @param d raw data to extract.
//' @param c an index of color channel to load.
//' @return The numeric matrix of the single channel extracted from d.
// [[Rcpp::export]]
NumericMatrix ExtractSingleChannelR(RawVector d, int c)
{
  NumericMatrix m_null;
  if(!isNd2loadedWithMsg()) {
    return m_null;
  }
  if(c <= 0 || c > info->C) {
    printf("C out of range\n");
    return m_null;
  }

  unsigned char* p = d.begin();
  NumericMatrix m(info->X, info->Y);
  double value;
  double n = pow(2, info->BitsPerPixel) - 1;
  value = *(((unsigned short*)p) + (c - 1));
  for(int y = 0; y < info->Y; y++) {
    for(int x = 0; x < info->X; x++) {
      if(info->PixelSize == 1) {
        value = *(p + (c - 1));
        p += info->C;
      } else {
        value = *(((unsigned short*)p) + (c - 1));
        p += info->C * 2;
      }
      // normalize intensity between 0 and 1 before assignment
      m(x, y) = value / n;
    }
  }
  if(show_hints) {
    printf("HINT: If you use EBImage, 'as.Image(im_ch)' or 'rgbImage(im_ch1, im_ch2, im_ch3)' is available\n");
  }

  return m;
}

//' Free the nd2 File
//' @description Free the nd2 file from memory.
// [[Rcpp::export]]
void FreeNd2R()
{
  if(info != NULL) {
    FreeNd2(info);
    info = NULL;
  }
  return;
}
