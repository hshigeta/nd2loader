#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "nd2loader.h"

int CheckNd2Info(nd2info*);
int nd2_debug_mode = 0;

unsigned char MAGIC_NUMBER[] = {0xDA, 0xCE, 0xBE, 0x0A};

void dbg_printf(const char *fmt, ...)
{
    va_list args;
    if(nd2_debug_mode) {
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);
    }
}

// compare two type of strings like 'u.i.*.*.*' and 'ui***'
int memcmp2(unsigned char* p, const char* pattern)
{
    unsigned int i;
    for(i = 0; i < strlen(pattern); i++) {
        if(*(p + i * 2) != pattern[i]) {
            return -1;
        }
    }
    return 0;
}

int ParseImageAttributesLV(nd2info* info, const char* filename, unsigned long offset, long size)
{
    FILE* fp;
    unsigned char* data;
    long i;

    if ((fp = fopen(filename, "rb")) == NULL) {
        return -1;
    }

    data = (unsigned char*)malloc(size);
    fseek(fp, offset, SEEK_SET);
    fread(data, sizeof(unsigned char), size, fp);

    for(i = 0; i < size - 36; i++) {
        // ignore "uiWidth'B'ytes"
        if(memcmp2(&data[i], "uiWidth") == 0 && data[i + 14] != 'B') {
            memcpy(&info->X, &data[i + 16], 2);
            dbg_printf("  uiWidth : %d\n", info->X);
        }
        if(memcmp2(&data[i], "uiHeight") == 0) {
            memcpy(&info->Y, &data[i + 18], 2);
            dbg_printf("  uiHeight : %d\n", info->Y);
        }
        if(memcmp2(&data[i], "uiComp") == 0) {
            memcpy(&info->C, &data[i + 14], 2);
            dbg_printf("  uiComp : %d\n", info->C);
        }
        if(memcmp2(&data[i], "uiBpcInMemory") == 0) {
            memcpy(&info->PixelSize, &data[i + 28], 2);
            info->PixelSize /= 8; // bits to byte(s)
            dbg_printf("  uiBpcInMemory : %d\n", info->PixelSize);
        }
        if(memcmp2(&data[i], "uiBpcSignificant") == 0) {
            memcpy(&info->BitsPerPixel, &data[i + 34], 2);
            dbg_printf("  uiBpcSignificant : %d\n", info->BitsPerPixel);
        }
    }

    fclose(fp);
    free(data);

    return 0;
}

int ParseImageTextInfoLV(nd2info* info, const char* filename, unsigned long offset, long size)
{
    FILE* fp;
    unsigned char* data;
    long i;
    unsigned long j, k, key, value;
    char buf[256];
    value = 1; // initialize just in case

    if ((fp = fopen(filename, "rb")) == NULL) {
        return -1;
    }

    data = (unsigned char*)malloc(size);
    fseek(fp, offset, SEEK_SET);
    fread(data, sizeof(unsigned char), size, fp);

    for(i = 0; i < size - 60; i++) {
        // ignore "uiWidth'B'ytes"
        if(memcmp2(&data[i], "Dimensions:") == 0) {
            dbg_printf("  'Dimensions' found : ");
            // "T.(.4.). .x. .Z.(.1.0.)." to "T(4) x Z(10)"
            for(j = 0; j < 256; j++) {
                if(data[i + 22 + j * 2] == 0x0D || data[i + 22 + j * 2] == 0x0A) {
                    break;
                }
            }
            for(k = 0; k < j; k++) {
                buf[k] = data[i + 22 + k * 2];
            }
            buf[j] = 0;
            dbg_printf("%s\n", buf);

            // parse info
            key = 0;
            for(k = 0; k < j; k++) {
                if(buf[k] == ' ') key = k + 1;
                if(buf[k] == '(') {
                    buf[k] = 0;
                    value = k + 1;
                }
                if(buf[k] == ')') {
                    buf[k] = 0;
                    if(buf[key] == 'Z') {
                        info->Z = atoi(&buf[value]);
                        dbg_printf("  Z : %d\n", info->Z);
                    }
                    if(buf[key] == 'T') {
                        info->T = atoi(&buf[value]);
                        dbg_printf("  T : %d\n", info->T);
                    }
                }
            }
            break;
        }
    }

    fclose(fp);
    free(data);

    return 0;
}


// whether printing debug info
void Nd2SetDebugMode(int mode)
{
    nd2_debug_mode = mode;
}

// load nd2 file and parse it
int ParseNd2Core(const char* filename, nd2info* info, bool auto_meta)
{
    FILE* fp;
    int i, ret, path, name_length, cur_pos, section_idx;
    long pos, data_length;
    unsigned char buf[BUF_SIZE];
    char section_name[MAX_SECTION_NAME];

    // open file
    if ((fp = fopen(filename, "rb")) == NULL) {
        return -1;
    }

    // initialize info just in case meta data is not retrieved
    if(auto_meta) {
        info->X = 1;
        info->Y = 1;
        info->Z = 1;
        info->T = 1;
        info->C = 1;
        info->BitsPerPixel = 8;
        info->PixelSize = 1;
    }

    // two-pass parsing (precheck the number of images and malloc/load)
    cur_pos = 0;
    info->image_num = 0;

    section_idx = -1;

    for(path = 0; path <= 1; path++)
    {
        fseek(fp, 0, SEEK_SET);
        if(path == 1) {
            // allocate memory according to the image num
            info->imlist = (struct nd2im*)malloc(sizeof(struct nd2im) * info->image_num);
        }
        while ((ret = fread(buf, sizeof(unsigned char), BUF_SIZE, fp)) > 0) {
            //dbg_printf("%s", buf);
            for(i = 0; i < ret; i++) {
                // search the magic number '0xDA 0xCE 0xBE 0x0A'
                if(i + 3 < ret && memcmp(&buf[i], MAGIC_NUMBER, 4) == 0) {
                    // the magic number was found
                    // load the section name/size, data name/size
                    section_idx++;
                    memcpy(&name_length, buf + i + 4, 4); // 4 bytes
                    memcpy(&data_length, buf + i + 8, 8); // 8 bytes
                    pos = ftell(fp) - ret + i;
                    strcpy(section_name, (const char*)buf + i + 16);
                    //dbg_printf("%08lX: %d, %ld\n%s\n", pos, name_length, data_length, section_name);

                    if(path == 0) {

                        // show section info
                        dbg_printf("%04d: %08lX %s (%ld bytes)\n", section_idx, pos, section_name, data_length);

                        // count the num of the images
                        if(strncmp(section_name, "ImageDataSeq|", 13) == 0) {
                            info->image_num++;
                        }

                        // search meta info (X, Y, C, Z, T, BitsPerPixel, PixelType)
                        if(auto_meta) {
                            if(strncmp(section_name, "ImageAttributesLV", 17) == 0) {
                                //dbg_printf("ImageAttributesLV Found : %04d\n", section_idx);
                                ParseImageAttributesLV(info, filename, pos + 16 + name_length, data_length);
                            }
                            if(strncmp(section_name, "ImageTextInfoLV", 15) == 0) {
                                //dbg_printf("ImageTextInfoLV Found : %04d\n", section_idx);
                                ParseImageTextInfoLV(info, filename, pos + 16 + name_length, data_length);
                            }
                        }

                    } else { // path = 1

                        if(strncmp(section_name, "ImageDataSeq|", 13) == 0) {
                            strcpy(info->imlist[cur_pos].name, section_name);
                            info->imlist[cur_pos].offset = pos + 16 + name_length;
                            info->imlist[cur_pos].size = data_length;
                            cur_pos++;
                        }
                    }
                }
            }
        }
    }
    fclose(fp);

    return 0;
}

nd2info* ParseNd2(const char* filename)
{
    struct nd2info* info = (struct nd2info*)malloc(sizeof(struct nd2info));
    info->imlist = NULL;

    if(ParseNd2Core(filename, info, true) < 0) {
        FreeNd2(info);
        return NULL;
    }

    // store meta data
    strcpy(info->filename, filename);

    // assertion
    CheckNd2Info(info);

    return info;
}

nd2info* ParseNd2WithMetaInfo(const char* filename, int X, int Y, int C, int Z, int T, int BitsPerPixel, int PixelSize)
{
    struct nd2info* info = (struct nd2info*)malloc(sizeof(struct nd2info));
    info->imlist = NULL;

    if(ParseNd2Core(filename, info, false) < 0) {
        FreeNd2(info);
        return NULL;
    }

    // store meta data
    strcpy(info->filename, filename);
    info->X = X;
    info->Y = Y;
    info->C = C;
    info->Z = Z;
    info->T = T;
    info->BitsPerPixel = BitsPerPixel;
    info->PixelSize = PixelSize;

    // assertion
    CheckNd2Info(info);

    return info;
}

// show information inside 'nd2info' structure
void ShowNd2info(nd2info* info)
{
    printf("Filename: %s\n", info->filename);
    printf("Dimension (X, Y, C, Z, T): (%d, %d, %d, %d, %d)\n", info->X, info->Y, info->C, info->Z, info->T);
    printf("BitsPerPixel: %d\n", info->BitsPerPixel);
    printf("PixelSize: %d\n", info->PixelSize);
    printf("Image Num: %d\n", info->image_num);

    CheckNd2Info(info);

    if(nd2_debug_mode) {
        printf("\nImage Sequences:\n");
        for(int i = 0; i < info->image_num; i++) {
            printf("%08lX %s (%ld bytes)\n", info->imlist[i].offset, info->imlist[i].name, info->imlist[i].size);
        }
    }

}

// display warning when nd2 params are not correct compared to the parsed information
int CheckNd2Info(nd2info* info)
{
    int ok = 1;
    if(info->image_num > 0) {
        if(info->Z * info->T != info->image_num) {
            printf("WARNING: Z (%d) * T (%d) = %d does not much the parsed image num (%d)\n",
            info->Z, info->T, info->Z * info->T, info->image_num);
            ok = 0;
        }

        int nd2_size = info->X * info->Y * info->C * info->PixelSize;
        int parsed_size = info->imlist[0].size - SEQDATA_IGNORE;
        if(nd2_size != parsed_size) {
            printf("WARNING: X (%d) * Y (%d) * C (%d) * PixelSize (%d) = %d does not much the parsed size (%d)\n",
            info->X, info->Y, info->C, info->PixelSize, nd2_size, parsed_size);
            ok = 0;
        }
    }
    return ok;
}


unsigned char* LoadRawDataIndex(nd2info* info, int index)
{
    FILE* fp;
    unsigned char* data;

    if(info == NULL) {
        printf("'info' is NULL\n");
        return NULL;
    }

    if(index <= 0 || index > info->image_num) {
        printf("index out of range\n");
        return NULL;
    }

    if ((fp = fopen(info->filename, "rb")) == NULL) {
        printf("failed to open file\n");
        return NULL;
    }

    data = (unsigned char*)malloc(info->imlist[index - 1].size - SEQDATA_IGNORE);
    fseek(fp, info->imlist[index - 1].offset + SEQDATA_IGNORE, SEEK_SET);
    fread(data, sizeof(unsigned char), info->imlist[index - 1].size - SEQDATA_IGNORE, fp);
    fclose(fp);

    return data;
}

unsigned char* LoadRawData(nd2info* info, int Z, int T)
{
    if(info == NULL) {
        printf("'info' is NULL\n");
        return NULL;
    }

    if(Z <= 0 || Z > info->Z) {
        printf("Z out of range\n");
        return NULL;
    }
    if(T <= 0 || T > info->T) {
        printf("T out of range\n");
        return NULL;
    }

    return LoadRawDataIndex(info, (T - 1) * info->T + (Z - 1) + 1);
}

void FreeNd2(struct nd2info* info)
{
    if(info != NULL) {
        if(info->imlist != NULL) {
            free(info->imlist);
        }
        free(info);
    }
    return;
}
