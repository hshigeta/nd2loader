# README #

This library has been created to load images from Nikon nd2 files. In order to utilize this library in various platforms, the core functions are written in C language. Also the R wrapper for the library is available at the moment.

CAUTION: I implemented as little as possible to load just our data,
so not all types of nd2 files are supported. Sorry!

### Limitation: ###

* Newer nd2 format (with uncompressed) only
* Size (X, Y, C) of each frame must be the same as others
* XYCZT order only
* The parse logic is not perfect (some nd2 files could not be loaded) - changing BUFSIZE might solve the problem

### Not Tested: ###

* Images with pixel size = 1
* Big endian environment (does not work?)
* Some platforms: 32 bit environments, Windows + Visual Studio, etc.

Let me know if you have troubles.


# Usage #

## C/C++ Language ##

(usage is coming soon)

## R Language ##

This library is distributed in a source package style.
You need to compile it beforehand using 'Rcpp' package in your platform.
For Windows, you also need 'Rtools' to build the source package.


### How To Install ###

* Install Rcpp first.

```
#!R
install.packages("Rcpp")
```

* (Windows Only) [Download](https://cran.r-project.org/bin/windows/Rtools/) Rtools and install it.
Do not forget to let the installer edit the system PATH.

The following two methods are available to install nd2loader.

#### 1. Devtools automatic installation ####

Enter following code.

```
#!R
install.packages("devtools")
devtools::install_bitbucket("hshigeta/nd2loader/nd2loaderR")
```

#### 2. Manual installation ####

* Install Rstudio (optional, but strongly recommended)
* Clone this repository
* Run Rstudio and select [File] -> [Open Project] and open 'nd2loaderR.Rproj'
* Select [Build] -> [Build and Reload], and compile will start
* After compiling the package, you will be able to load it

### How To Use ###

Please use RStudio for now. (System messages are not displayed other than RStudio)

```
#!R

# Load package
library(nd2loaderR)

# if you would like, you can enable a function to output hints.
Nd2SetHintR(1)

# Parse a nd2 file (actual image data is not loaded at this method)
ParseNd2R("yourfile.nd2")

# If an error occurs, confirm the current directory and that file exists.
# Even if ParseNd2R does not work correctly, you can try parsing with meta information.
# ParseNd2WithMetaInfoR(filename, X, Y, C, Z, T, BitPerPixel, PixelSize)
ParseNd2WithMetaInfoR("yourfile.nd2", 512, 512, 4, 4, 90, 12, 2)

# Show the information ('Image Num' is extracted from the parsing result)
ShowNd2infoR()

# Extract each image data
# LoadRawDataR(Z, T)
raw <- LoadRawDataR(1, 1)

# (also possible to use a serial index)
# raw <- LoadRawDataIndexR(1)

# Get a matrix from raw byte array
# ExtractSingleChannelR(raw, ChannelIndex)
im_ch1 <- ExtractSingleChannelR(raw, 1)
im_ch2 <- ExtractSingleChannelR(raw, 2)
im_ch3 <- ExtractSingleChannelR(raw, 3)

# Following examples need 'EBImage' package (install it beforehand)
library(EBImage)

# Example 1: Create a gray-scale image
im_gray <- as.Image(im_ch1)
display(im_gray)

# Example 2: Create a colored image
im_color <- rgbImage(im_ch1, im_ch2, im_ch3)
display(im_color)

# Free nd2 meta info from memory
FreeNd2R()

```